// Type definitions for Kaiba 1.0.0
// Project: Kaiba
// Definitions by: Tristan Chatman

/// <reference path="markdown.d.ts" />
/// <reference path="sfc.d.ts" />

import VueRouter from 'vue-router';
import Vue, { VueConstructor } from 'vue';

export declare interface Content {
  id: string | number
  title: string
  body: string
  uri: string
  contentType: string
}

export declare interface ThemeArguments {
  templates: Map<string, any>;
  defaultTemplate: any;
  homeTemplate: any;
  errorTemplate: any;
}

export declare interface KaibaPlugin {
  install(Kaiba: Function, args: any): void
}

export declare abstract class ContentManager {
  constructor(args: object);
  static init(args: object): ContentManager;
  abstract getByID(id: string | number): Content;
  abstract getByURI(uri: string): Content;
}

export declare class Template extends Vue { }
export declare class ErrorTemplate extends Vue { }

export declare class ContentNotFoundError extends Error { constructor(contentPath: string); }
export declare class MalformedMarkdownError extends Error { constructor(m: string); }
export declare class TemplateNotFoundError extends Error { constructor(contentType: string); }

export declare class MarkdownRenderer extends Vue {
  output: string
}

export declare class Kaiba {
  constructor(args: any);
  router: VueRouter;
  content: ContentManager;
  static ContentManagerImplementation: any;
  static plugins: Map<KaibaPlugin, any>;
  install(Vue: VueConstructor<Vue>, args: object): void;
}

export declare class RouteManager extends Vue {
  currentTemplate: VueConstructor<Template> | null;
  currentContent: Content | null;
  currentError: Error | null;
}

export declare class MarkdownContent implements Content {
  constructor(id: string | number, markdown: string, uri: string);
  id: string | number;    
  title: string;
  body: string;
  uri: string;
  contentType: string;
}

export declare class MarkdownParser {
  constructor();
  render(markdown: string);
  static getInstance(): MarkdownParser;
}

export declare class FlatFileContentManager extends ContentManager {
  constructor(args: any);
  getByID(id: string | number): Content; 
  getByURI(uri: string): Content;
  static init(args: object): FlatFileContentManager;
}

export declare class Theme implements KaibaPlugin {
  templates: Map<string, VueConstructor<Template>>;
  defaultTemplate: VueConstructor<Template>;
  homeTemplate: VueConstructor<Template>;
  errorTemplate: VueConstructor<ErrorTemplate>;
  install(Kaiba: Function, args: any): void;
}
