import { MarkdownContent } from "../../src/content/MarkdownContent";
import { MalformedMarkdownError } from "../../src/errors/MalformedMarkdownError";

import HelloWorldContent from "../resources/content/1.Hello-World.md";
import EmptyFrontMatterContent from "../resources/content/2.empty-front-matter.md";
import MissingFrontMatterContent from "../resources/content/3.missing-front-matter.md";

describe('MarkdownContent', () => {

  let id: number
  let uri: string

  beforeEach(() => {
    id = Math.random() * 99999
    uri = '/'
  })
  
  it('should contain all required properties of the Content interface', () => {
    let sut = new MarkdownContent(id, HelloWorldContent, uri)
    
    expect(sut.id).toBeDefined()
    expect(sut.uri).toBeDefined()
    expect(sut.contentType).toBeDefined()
    expect(sut.title).toBeDefined()
    expect(sut.body).toBeDefined()
  })

  it('should throw a MalformedMarkdownError when front-matter is missing', () => {
    let construction = () => {
      new MarkdownContent(id, EmptyFrontMatterContent, uri)
    }
    expect(construction).toThrowError(MalformedMarkdownError)
  })

  it('should throw a MalformedMarkdownError when missing requried front-matter fields', () => {
    let construction = () => {
      new MarkdownContent(id, MissingFrontMatterContent, uri)
    }
    expect(construction).toThrowError(MalformedMarkdownError)
  })
})