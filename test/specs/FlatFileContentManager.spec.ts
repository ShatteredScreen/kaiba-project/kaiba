import { FlatFileContentManager } from "../../src/contentmanagers/FlatFileContentManager";
import { MarkdownContent } from "../../src/content/MarkdownContent"
import HelloWorldContent from "../resources/content/1.Hello-World.md";

describe('FlatFIleContentManager', () => {
  
  let markup: Map<String, String> = new Map([
    ['/', HelloWorldContent],
  ])
  let sut: FlatFileContentManager

  beforeEach(() => {
    sut = FlatFileContentManager.init({
      markup: markup
    })
  })

  it('returns an instance of MarkdownContent when you retrieve one by ID', () => {
    let content = sut.getByID(0)
    expect(content).toBeInstanceOf(MarkdownContent)
  })

  it('returns an instance of MarkdownContent when you retrieve one by URI', () => {
    let content = sut.getByURI('/')
    expect(content).toBeInstanceOf(MarkdownContent)
  })
})