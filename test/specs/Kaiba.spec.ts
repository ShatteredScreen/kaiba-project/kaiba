import { Kaiba } from "../../src/Kaiba";
import { KaibaPlugin } from "../../src/plugin/KaibaPlugin"
import { ContentManager } from "../../src/contentmanagers/ContentManager";
import VueRouter from 'vue-router';
import { createLocalVue, mount } from "@vue/test-utils"
import Vue, { VueConstructor } from "vue";
import EmptyComponent from "../resources/EmptyComponent.vue"

describe('Kaiba', () => {

  let sut: Kaiba
  let plugin: any

  let localKaiba: any
  let localVue: VueConstructor<Vue>

  beforeEach(() => {
    sut = new Kaiba({})

    localKaiba = Kaiba
    localVue = createLocalVue()

    plugin = jest.mock('../../src/plugin/KaibaPlugin', () => {
      install: (Kaiba: Function, args: any) => { return }
    })
  })

  it('should be accessible from within a Vue instance when installed', () => {
    // Install our instance of Kaiba
    localVue.use(sut)
    let wrapper = mount(EmptyComponent, { localVue })
    let vm: any = wrapper.vm

    expect(vm.$kaiba).toBeDefined()
  })
  
  it('should return a ContentManager instance', () => {
    let contentManager = sut.content
    expect(contentManager).toBeInstanceOf(ContentManager)
  })

  it('should contain all installed plugins and plugin arguments', () => {
    localKaiba.use(plugin, { coolStuff: true })
    expect(localKaiba.plugins).toEqual(new Map([[plugin, { coolStuff: true }]]))
  })
})