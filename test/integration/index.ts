import Vue from "vue";

import { Kaiba } from "../../src/Kaiba";
import { Theme } from "../../src/theme/Theme";

import App from "./App.vue";

import HelloWorldContent from "../resources/content/1.Hello-World.md";
import theme from "../resources/theme";

let markup = new Map([
  ['/', HelloWorldContent]
])

Kaiba.use(theme)
let kaiba = new Kaiba({ 
  theme: {},
  markup: markup
})
Vue.use(kaiba)

new Vue({
  render: h => h(App), 
  el: '#app',
  router: kaiba.router,
})