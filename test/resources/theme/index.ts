import { Theme } from "../../../src/theme/Theme";
import DefaultTemplate from "./DefaultTemplate.vue";
import HomeTemplate from "./HomeTemplate.vue";
import PageNotFoundTemplate from "./PageNotFoundTemplate.vue";

export default new Theme({
  templates: new Map([
    ['Page', DefaultTemplate]
  ]),
  defaultTemplate: DefaultTemplate,
  homeTemplate: HomeTemplate,
  errorTemplate: PageNotFoundTemplate
})