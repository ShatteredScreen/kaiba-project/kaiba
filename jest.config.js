module.exports = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  moduleFileExtensions: [ "js", "ts", "json", "vue" ],
  transform: {
    ".*\\.js$": "babel-jest",
    ".*\\.(vue)$": "vue-jest",
    ".*\\.md$": "jest-raw-loader"
  },
};