export class TemplateNotFoundError extends Error {

  constructor(contentType: string) {
    super(`There was no matching template that could handle the content-type of ${contentType}`)
  }
}