export class MalformedMarkdownError extends Error {
  constructor(m: string) {
    super(m)
  }
}