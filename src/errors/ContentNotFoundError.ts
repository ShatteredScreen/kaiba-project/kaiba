export class ContentNotFoundError extends Error {

  constructor(contentPath: string) {
    super(`There was no content with the matching path of ${contentPath}`)
  }
}