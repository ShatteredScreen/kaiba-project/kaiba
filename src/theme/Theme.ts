import Vue, { VueConstructor } from 'vue'
import { KaibaPlugin } from '../plugin/KaibaPlugin'
import Template from '../components/Template.vue'
import ErrorTemplate from '../components/Template.vue'

interface ThemeArguments {
  templates: Map<string, any>
  defaultTemplate: any
  homeTemplate: any
  errorTemplate: any
}

export class Theme implements KaibaPlugin {
  templates: Map<string, VueConstructor<Template>> = new Map()
  defaultTemplate: VueConstructor<Template>
  homeTemplate: VueConstructor<Template>
  errorTemplate: VueConstructor<ErrorTemplate>

  constructor(args: ThemeArguments) {
    this.templates = args.templates
    this.defaultTemplate = args.defaultTemplate
    this.homeTemplate = args.homeTemplate
    this.errorTemplate = args.errorTemplate
  }

  install(Kaiba: Function, args: any): void {
    let theme = this
    Object.defineProperties(Kaiba.prototype, {
      theme: {
        get() { return theme }
      },
    })
  }

  getTemplate(contentType: string): VueConstructor<Template> {
    let template = this.defaultTemplate
    
    if (this.templates.has(contentType)) {
      template = this.templates.get(contentType)
    } 

    return template
  }
}