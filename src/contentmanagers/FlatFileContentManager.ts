import { ContentManager } from "./ContentManager"
import { Content } from "../content/Content"
import { MarkdownContent } from "../content/MarkdownContent";

export class FlatFileContentManager extends ContentManager {

  private contentCollection: Array<Content> = []

  constructor(args: any) {
    super(args)
    if (args.markup) {
      this.loadMarkup(args.markup)
    }
  }
  
  getByID(id: string | number): Content {
    let contentMatched = this.contentCollection.filter(content => content.id === id)
    return contentMatched[0]
  }
  
  getByURI(uri: string): Content {
    let contentMatched = this.contentCollection.filter(content => content.uri === uri)
    return contentMatched[0]
  }

  private loadMarkup(markupCollection: Array<string> | Map<string, string>) {
    for (let markup of markupCollection) {
      if (typeof markup === 'string') {
        this.buildContent(markup)
      }

      else {
        this.buildContent(markup[1], markup[0])
      }
    }
  }

  private buildContent(markup: string, uri: string = '') {
    let contentID = this.generateContentID(this.contentCollection.length)
    uri = uri ? uri : `/${contentID}`
    let content = new MarkdownContent(contentID, markup, uri)
    this.contentCollection.push(content)
  }

  private generateContentID(position: number): number {
    return position
  }

  static init(args: object): FlatFileContentManager {
    return new FlatFileContentManager(args)
  }
}