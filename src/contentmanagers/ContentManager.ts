import { Content } from "../content/Content";

export abstract class ContentManager {
  constructor(args: object) {}
  static init(args: object): ContentManager { throw new Error("Method not implemented.") }
  abstract getByID(id: string | number): Content
  abstract getByURI(uri: string): Content
}