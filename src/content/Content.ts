export interface Content {
  id: string | number
  title: string
  body: string
  uri: string
  contentType: string
}