import { Content } from "./Content";
import { MalformedMarkdownError } from "../errors/MalformedMarkdownError";
import * as matter from "gray-matter";


export class MarkdownContent implements Content {

  id: string | number
  uri: string
  contentType: string = 'Page'
  title: string
  body: string

  constructor(id: string | number, markdown: string, uri: string) {
    let objectifiedMarkdown = (matter as any).default(markdown)
    let fields: { [key: string]: any } = objectifiedMarkdown.data
    
    if (!('title' in fields) || !('contentType' in fields) || fields.title == null || fields.contentType == null) {
      let malformedErrorMsg = `Markdown front-matter is missing one or more of the require fields.`
      throw new MalformedMarkdownError(malformedErrorMsg)
    }

    this.id = id
    this.uri = uri
    this.contentType = fields.contentType
    this.title = fields.title
    this.body = objectifiedMarkdown.content
  }
}