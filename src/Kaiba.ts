import VueRouter from 'vue-router'

import { FlatFileContentManager } from './contentmanagers/FlatFileContentManager'
import { ContentManager } from './contentmanagers/ContentManager'
import { KaibaPlugin } from './plugin/KaibaPlugin'
import { Theme } from './theme/Theme'

import MarkdownRenderer from './components/MarkdownRenderer.vue'
import RouteManager from './components/RouteManager.vue'
import Vue, { VueConstructor, PluginObject } from "vue";


export class Kaiba {
  
  static ContentManagerImplementation: any = FlatFileContentManager
  static plugins: Map<KaibaPlugin, any> = new Map() 

  private contentManager: ContentManager
  private routes: Array<any> = [
    { path: '/*', component: RouteManager }
  ]

  theme?: Theme

  constructor(args: any) {
    Kaiba.installPlugins()
    this.contentManager = Kaiba.initContentManager(args)
  }

  /**
   * The install function of this Vue Plugin. 
   * Load in any depenecies and components.
   * 
   * @param {*} Vue 
   * @param {object} args 
   * @memberof Kaiba
   */
  install(Vue: VueConstructor<Vue>, args: object) {
    // Install components and Vue dependencies
    this.installVuePlugins(Vue)
    this.installComponents(Vue)

    // Make Kaiba accessible
    let kaiba = this
    Object.defineProperties(Vue.prototype, {
      $kaiba: {
        get() { return kaiba }
      },

      // See you space shitlords
      $zubaz: {
        get() { return "We expected nothing, and you gave us everything (2011-2018)" }
      }
    })
  }

  /**
   * Install depended Vue Plugins
   * 
   * @private
   * @param {*} Vue 
   * @memberof Kaiba
   */
  private installVuePlugins(Vue: VueConstructor<Vue>) {
    Vue.use(VueRouter)
  }

  /**
   * Make our Vue components App wide
   * 
   * @private
   * @param {*} Vue 
   * @memberof Kaiba
   */
  private installComponents(Vue: VueConstructor<Vue>) {
    Vue.component('md', MarkdownRenderer);
  }

  /**
   * Loop through the array of registered plugins for Kaiba and run their installers
   * 
   * @private
   * @static
   * @memberof Kaiba
   */
  private static installPlugins() {
    for (let [plugin, args] of Kaiba.plugins) {
      plugin.install(Kaiba, args)
    }
  }

  /**
   * Initialize the implementation of the content manager class
   * 
   * @private
   * @static
   * @param {object} args 
   * @returns 
   * @memberof Kaiba
   */
  private static initContentManager(args: object) {
    return Kaiba.ContentManagerImplementation.init(args)
  }

  /**
   * Register a plugin to be used with Kaiba
   * 
   * @static
   * @param {KaibaPlugin} plugin 
   * @param {object} [args={}] 
   * @returns
   * @memberof Kaiba
   */
  static use(plugin: KaibaPlugin, args: object = {}) {
    Kaiba.plugins.set(plugin, args);
  }

  /**
   * Get an instance of the VueRouter with our paths loaded into it
   * 
   * @readonly
   * @type {VueRouter}
   * @memberof Kaiba
   */
  get router(): VueRouter {
    return new VueRouter({
      mode: 'history',
      routes: this.routes
    })
  }

  /**
   * Get the current implementation of the content manager
   * 
   * @readonly
   * @type {ContentManager}
   * @memberof Kaiba
   */
  get content(): ContentManager {
    return this.contentManager
  }
}