import { Kaiba } from "../Kaiba";

export interface KaibaPlugin {
  install(Kaiba: Function, args: any): void
}