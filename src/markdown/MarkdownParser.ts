import { Converter } from 'showdown'

/**
 * A Singleton wrapper for the Showdown markdown renderer
 * 
 * @class MarkdownTransformer
 */
export class MarkdownParser {
  private static instance: MarkdownParser
  private converter: Converter

  constructor() {
    this.converter = new Converter()
  }

  static getInstance(): MarkdownParser {
    if (!MarkdownParser.instance) {
      MarkdownParser.instance = new MarkdownParser();
    }
    return MarkdownParser.instance;
  }

  render(markdown: string) {
    return this.converter.makeHtml(markdown)
  }
}