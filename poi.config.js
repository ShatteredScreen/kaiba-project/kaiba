module.exports = {
  entry: './test/integration/index.ts',
  plugins: [
    { resolve: '@poi/plugin-typescript' }
  ],
  chainWebpack(config) {
    // Add Markdown
    config.module.rule('markdown')
      .test(/\.md$/)
      .use('raw')
      .loader(require.resolve('raw-loader'))
      .end()

    return config
  }
}